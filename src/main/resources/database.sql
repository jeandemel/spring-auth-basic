-- Active: 1688384959620@@127.0.0.1@3306@spring_jdbc_auth
DROP TABLE IF EXISTS user;

CREATE TABLE user (
    id INT PRIMARY KEY AUTO_INCREMENT,
    email VARCHAR(255) NOT NULL,
    password VARCHAR(255) NOT NULL,
    role VARCHAR(50) NOT NULL
);

INSERT INTO `user` (email,password,role) VALUES 
('test@test.com','$2a$12$MZh4.klAgZqlGMFHOZp3UOy37AvFwqPKAdeIX6USIJYYmo3FPPtlO', 'ROLE_USER'),
('admin@test.com','$2a$12$MZh4.klAgZqlGMFHOZp3UOy37AvFwqPKAdeIX6USIJYYmo3FPPtlO', 'ROLE_ADMIN');
