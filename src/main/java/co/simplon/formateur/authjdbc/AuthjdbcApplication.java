package co.simplon.formateur.authjdbc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AuthjdbcApplication {

	public static void main(String[] args) {
		SpringApplication.run(AuthjdbcApplication.class, args);
	}

}
