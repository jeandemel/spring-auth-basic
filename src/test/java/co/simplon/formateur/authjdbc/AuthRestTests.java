package co.simplon.formateur.authjdbc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;


@SpringBootTest
@Sql("/database.sql")
@AutoConfigureMockMvc
public class AuthRestTests {
    @Autowired
    MockMvc mvc;


    @Test
    void testRegisterSuccess() throws Exception {
        mvc.perform(
            post("/api/user")
            .contentType(MediaType.APPLICATION_JSON)
            .content("""
                {
                    "email": "mail@mail.com",
                    "password":"1234"
                }
            """)
            ).andExpect(status().isCreated())
            .andExpect(jsonPath("$['id']").isNumber());

    }
    
    @Test
    void testRegisterAlreadyExists() throws Exception {
        mvc.perform(
            post("/api/user")
            .contentType(MediaType.APPLICATION_JSON)
            .content("""
                {
                    "email": "test@test.com",
                    "password":"1234"
                }
            """)
            ).andExpect(status().isBadRequest());

    }

    @Test
    @WithUserDetails("test@test.com")
    void testLoginSuccess() throws Exception {
        mvc.perform(get("/api/account"))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$['id']").isNumber())
        .andExpect(jsonPath("$['email']").isString())
        .andExpect(jsonPath("$['role']").isString());
    }

    @Test
    @WithUserDetails("test@test.com")
    void testProtectedSuccess() throws Exception {
        mvc.perform(get("/api/protected"))
        .andExpect(status().isOk());
    }
    @Test
    void testProtectedForbidden() throws Exception {
        mvc.perform(get("/api/protected"))
        .andExpect(status().isUnauthorized());
    }
}
